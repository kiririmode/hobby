variable "notification_emails" {
  type        = list(string)
  description = "予算アラートの通知先メールアドレスリスト"
}
